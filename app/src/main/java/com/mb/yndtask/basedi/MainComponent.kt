package com.mb.yndtask.basedi

import android.content.Context
import com.mb.yndtask.basedi.module.DbModule
import com.mb.yndtask.basedi.module.SchedulerModule
import com.mb.yndtask.galleryfragment.di.GalleryFragmentComponent
import com.mb.yndtask.loginfragment.di.LoginFragmentComponent
import com.mb.yndtask.registerfragment.di.RegisterFragmentComponent
import dagger.BindsInstance
import dagger.Component
import dagger.Provides
import dagger.Subcomponent

@Component(modules = [SchedulerModule::class, DbModule::class])
interface MainComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun bindContext(context: Context): Builder

        fun build(): MainComponent
    }

    fun plusLoginFragmentComponent(): LoginFragmentComponent
    fun plusRegisterFragmentComponent(): RegisterFragmentComponent
    fun plusGalleryFragmentComponentBuilder(): GalleryFragmentComponent.Builder
}