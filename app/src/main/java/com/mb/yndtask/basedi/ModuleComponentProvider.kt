package com.mb.yndtask.basedi

import android.content.Context

object ModuleComponentProvider {

    private var moduleComponent: MainComponent? = null

    fun buildModuleComponent(context: Context): MainComponent {
        if (moduleComponent == null) {
            moduleComponent = DaggerMainComponent.builder()
                .bindContext(context)
                .build()
        }
        return moduleComponent!!
    }

    fun provideModuleComponent(): MainComponent {
        if (moduleComponent == null) {
            throw IllegalStateException("Module component is null!")
        }
        return moduleComponent!!
    }

    fun detachComponent() {
        moduleComponent = null
    }
}