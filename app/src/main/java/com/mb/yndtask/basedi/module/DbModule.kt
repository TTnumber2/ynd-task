package com.mb.yndtask.basedi.module

import android.content.Context
import com.mb.yndtask.db.GalleryDB
import dagger.Module
import dagger.Provides

@Module
class DbModule {

    @Provides
    fun providesUserDao(context: Context) = GalleryDB.getInstance(context).userDAO()

    @Provides
    fun providesPhotoDao(context: Context) = GalleryDB.getInstance(context).photoDAO()
}