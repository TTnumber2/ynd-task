package com.mb.yndtask.basepresentation.dialog

import android.content.Context
import android.os.Bundle
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import com.mb.yndtask.databinding.DialogErrorBinding

class ErrorDialog(context: Context) : AlertDialog(context) {

    private var binding: DialogErrorBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DialogErrorBinding.inflate(layoutInflater)
        setContentView(binding!!.root)

        binding?.firstButton?.setOnClickListener { this.dismiss() }
    }

    fun setTitleText(res: String): ErrorDialog {
        binding?.title?.text = res
        return this
    }

    fun setMessageText(res: String): ErrorDialog {
        binding?.message?.text = res
        return this
    }

    fun showWithInstance(): ErrorDialog {
        super.show()
        return this
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        binding = null
    }
}