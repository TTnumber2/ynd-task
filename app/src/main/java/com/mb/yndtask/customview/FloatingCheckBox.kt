package com.mb.yndtask.customview

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import com.mb.yndtask.R
import kotlinx.android.synthetic.main.view_floating_check_box.view.*

class FloatingCheckBox : ConstraintLayout {

    private var isChecked: Boolean = false

    constructor(context: Context?) : super(context) {
        initView(context, null)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        initView(context, attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context,
                                                                                    attrs,
                                                                                    defStyleAttr) {
        initView(context, attrs)
    }

    private fun initView(context: Context?, attrs: AttributeSet?) {
        inflate(context, R.layout.view_floating_check_box, this)
        context!!.theme.obtainStyledAttributes(attrs, R.styleable.FloatingCheckBox, 0, 0).let { attributes ->
            label_placeholder.text = attributes.getString(R.styleable.FloatingCheckBox_cb_label) ?: ""
        }
    }

    fun setChecked(isChecked: Boolean) {
        icon_imageview.setImageResource(if (isChecked) R.drawable.ic_success else R.drawable.ic_error)
        this.isChecked = isChecked
    }

    fun isChecked() = isChecked
}