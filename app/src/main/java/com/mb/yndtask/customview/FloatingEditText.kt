package com.mb.yndtask.customview

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import com.jakewharton.rxbinding2.widget.RxTextView
import com.mb.yndtask.R
import kotlinx.android.synthetic.main.view_floating_edit_text.view.*

class FloatingEditText : ConstraintLayout {

    private val textObservable by lazy { RxTextView.textChangeEvents(edittext) }

    constructor(context: Context?) : super(context) {
        initView(context, null)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        initView(context, attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context,
                                                                                    attrs,
                                                                                    defStyleAttr) {
        initView(context, attrs)
    }

    private fun initView(context: Context?, attrs: AttributeSet?) {
        inflate(context, R.layout.view_floating_edit_text, this)
        context!!.theme.obtainStyledAttributes(attrs, R.styleable.FloatingEditText, 0, 0).let { attributes ->
            icon_imageview.setImageDrawable(attributes.getDrawable(R.styleable.FloatingEditText_icon_drawable))
            label_placeholder.text = attributes.getString(R.styleable.FloatingEditText_label) ?: ""
            edittext.hint = attributes.getString(R.styleable.FloatingEditText_hint) ?: ""
        }
    }

    fun setEmptyText() {
        edittext.setText("")
    }

    fun getText() = edittext.text.toString()

    fun getTextWatcherObservable() = textObservable.map { it.text().toString() }
}