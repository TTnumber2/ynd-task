package com.mb.yndtask.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.mb.yndtask.db.photo.dao.PhotoDAO
import com.mb.yndtask.db.photo.model.Photo
import com.mb.yndtask.db.user.dao.UserDAO
import com.mb.yndtask.db.user.model.User

@Database(entities = [User::class, Photo::class],
          version = 1)
abstract class GalleryDB : RoomDatabase() {

    abstract fun userDAO(): UserDAO
    abstract fun photoDAO(): PhotoDAO

    companion object {

        @Volatile
        private var INSTANCE: GalleryDB? = null

        fun getInstance(context: Context): GalleryDB = INSTANCE ?: synchronized(this) {
            INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(
                context, GalleryDB::class.java, "gallerydatabase.db").build()
    }
}