package com.mb.yndtask.db.photo.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.mb.yndtask.db.photo.model.Photo
import io.reactivex.Single

@Dao
interface PhotoDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(model: Photo): Long

    @Query("SELECT * FROM Photo WHERE userId = :userId")
    fun getUserPhotos(userId: Long): Single<List<Photo>>
}