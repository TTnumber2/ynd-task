package com.mb.yndtask.db.photo.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Photo")
data class Photo(@PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") var id: Long = 0L,
                 @ColumnInfo(name = "userId") var userId: Long,
                 @ColumnInfo(name = "photoPath") val photoPath: String,
                 @ColumnInfo(name = "photoCreationDate") val creationDate: Long,
                 @ColumnInfo(name = "photoUUID") val photoUUID: String)