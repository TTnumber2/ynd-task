package com.mb.yndtask.db.user.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.mb.yndtask.db.user.model.User
import io.reactivex.Single

@Dao
interface UserDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(model: User): Long

    @Delete
    fun delete(model: User)

    @Query("SELECT * FROM User WHERE login LIKE :username")
    fun getUser(username: String): Single<User>
}