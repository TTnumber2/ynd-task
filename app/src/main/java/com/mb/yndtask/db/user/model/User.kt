package com.mb.yndtask.db.user.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "User")
data class User(@PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") var id: Long = 0L,
                @ColumnInfo(name = "login") var login: String,
                @ColumnInfo(name = "password") var password: String)