package com.mb.yndtask.extensions

import androidx.recyclerview.widget.RecyclerView

fun RecyclerView.onUpDownScrollListener(onUpAction: () -> Unit, onDownAction: () -> Unit) {
    this.addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            if (dy > 0) {
                onUpAction.invoke()
            } else {
                onDownAction.invoke()
            }
        }
    })
}