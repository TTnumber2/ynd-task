package com.mb.yndtask.galleryfragment.di

import com.mb.yndtask.galleryfragment.presentation.GalleryFragment
import com.mb.yndtask.galleryfragment.presentation.adapterview.PhotoItem
import dagger.BindsInstance
import dagger.Subcomponent
import javax.inject.Qualifier

@Subcomponent(modules = [GalleryFragmentPresentationModule::class, GalleryFragmentDomainModule::class])
interface GalleryFragmentComponent {

    @Subcomponent.Builder
    interface Builder {

        @BindsInstance
        fun bindLoggedUser(@LoggedUser username: String): Builder

        fun build(): GalleryFragmentComponent

        @Qualifier
        annotation class LoggedUser
    }

    fun inject(fragment: GalleryFragment)
    fun inject(item: PhotoItem)
}