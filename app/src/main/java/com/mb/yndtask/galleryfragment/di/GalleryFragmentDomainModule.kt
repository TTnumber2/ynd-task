package com.mb.yndtask.galleryfragment.di

import com.mb.yndtask.galleryfragment.domain.GalleryInteractor
import com.mb.yndtask.galleryfragment.domain.GalleryInteractorImpl
import com.mb.yndtask.galleryfragment.domain.photoencrypt.PhotoSecret
import com.mb.yndtask.galleryfragment.domain.photoencrypt.PhotoSecretImpl
import dagger.Binds
import dagger.Module

@Module
abstract class GalleryFragmentDomainModule {

    @Binds
    abstract fun bindsInteractor(interctor: GalleryInteractorImpl): GalleryInteractor

    @Binds
    abstract fun bindsPhotoSecret(secret: PhotoSecretImpl): PhotoSecret
}