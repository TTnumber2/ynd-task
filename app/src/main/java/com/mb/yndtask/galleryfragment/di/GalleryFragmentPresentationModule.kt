package com.mb.yndtask.galleryfragment.di

import com.mb.yndtask.galleryfragment.viewmodel.GalleryFragmentMVVM
import com.mb.yndtask.galleryfragment.viewmodel.GalleryViewModel
import dagger.Binds
import dagger.Module

@Module
abstract class GalleryFragmentPresentationModule {

    @Binds
    abstract fun bindsViewModel(vm: GalleryViewModel): GalleryFragmentMVVM.ViewModel
}