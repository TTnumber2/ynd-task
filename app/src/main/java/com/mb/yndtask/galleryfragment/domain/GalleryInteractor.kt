package com.mb.yndtask.galleryfragment.domain

import com.mb.yndtask.db.photo.model.Photo
import com.mb.yndtask.galleryfragment.domain.model.PhotoDomain
import io.reactivex.Single

interface GalleryInteractor {

    fun getPhotosForUser(): Single<List<PhotoDomain>>
    fun savePhotoForUser(photo: Photo): Single<Long>
}