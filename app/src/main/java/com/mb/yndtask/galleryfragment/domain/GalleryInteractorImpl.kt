package com.mb.yndtask.galleryfragment.domain

import com.mb.yndtask.db.photo.dao.PhotoDAO
import com.mb.yndtask.db.photo.model.Photo
import com.mb.yndtask.db.user.dao.UserDAO
import com.mb.yndtask.galleryfragment.di.GalleryFragmentComponent.Builder.LoggedUser
import com.mb.yndtask.galleryfragment.domain.model.PhotoDomain
import io.reactivex.Single
import javax.inject.Inject

class GalleryInteractorImpl
@Inject constructor(@LoggedUser private val username: String,
                    private val userDAO: UserDAO,
                    private val photoDAO: PhotoDAO) : GalleryInteractor {

    override fun getPhotosForUser(): Single<List<PhotoDomain>> =
            userDAO.getUser(username)
                    .flatMap { photoDAO.getUserPhotos(it.id) }
                    .map { it.map { it.mapToDomain() } }

    private fun Photo.mapToDomain() =
            PhotoDomain(this.photoUUID, this.photoPath, this.creationDate)

    override fun savePhotoForUser(photo: Photo): Single<Long> =
            userDAO.getUser(username)
                    .map { photoDAO.insert(photo.apply { userId = it.id }) }
}