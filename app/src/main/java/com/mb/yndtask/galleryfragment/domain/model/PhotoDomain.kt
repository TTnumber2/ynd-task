package com.mb.yndtask.galleryfragment.domain.model

data class PhotoDomain(val photoUUID: String,
                       val photoPath: String,
                       val photoCreationDate: Long)