package com.mb.yndtask.galleryfragment.domain.photoencrypt

import io.reactivex.Completable

interface PhotoSecret {
    fun encryptImage(originalFilePath: String): Completable
}