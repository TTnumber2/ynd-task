package com.mb.yndtask.galleryfragment.domain.photoencrypt

import io.reactivex.Completable
import io.reactivex.Observable
import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.security.NoSuchAlgorithmException
import javax.crypto.Cipher
import javax.crypto.CipherInputStream
import javax.crypto.CipherOutputStream
import javax.crypto.SecretKey
import javax.crypto.spec.SecretKeySpec
import javax.inject.Inject

class PhotoSecretImpl
@Inject constructor() : PhotoSecret {

    companion object {
        private const val ALGORITHM = "AES"
        private const val salt = "u8x/A?D(G+KbPeSh"
        private const val TEMP_IMAGE_TAG = "temp_"
    }

    private var key: SecretKey? = null

    init {
        key = getKey()
    }

    private fun getKey(): SecretKey? {

        var secretKey: SecretKey? = null

        try {
            secretKey = SecretKeySpec(salt.toBytes(), ALGORITHM)
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }

        return secretKey
    }

    private fun String.toBytes(): ByteArray {
        return this.toByteArray(Charsets.UTF_8)
    }

    override fun encryptImage(originalFilePath: String): Completable {
        val encryptedImagePath = createCopyOfOriginalFile(originalFilePath)

        return Completable.create { emitter ->

            try {

                try {
                    val fis = FileInputStream(originalFilePath)
                    val aes = Cipher.getInstance(ALGORITHM)
                    aes.init(Cipher.ENCRYPT_MODE, key)
                    val fs = FileOutputStream(File(encryptedImagePath))
                    val out = CipherOutputStream(fs, aes)
                    out.write(fis.readBytes())
                    out.flush()
                    out.close()
                } catch (e: NoSuchAlgorithmException) {
                    e.printStackTrace()

                    if (!emitter.isDisposed) {
                        emitter.onError(e)
                        emitter.onComplete()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()

                    if (!emitter.isDisposed) {
                        emitter.onError(e)
                        emitter.onComplete()
                    }
                }

                deleteFile(originalFilePath)
                renameImageToOriginalFileName(encryptedImagePath)

                if (!emitter.isDisposed) {
                    emitter.onComplete()
                }

            } catch (e: Exception) {
                e.printStackTrace()

                if (!emitter.isDisposed) {
                    emitter.onError(e)
                    emitter.onComplete()
                }
            }
        }
    }

    fun decryptImage(originalFilePath: String): Observable<File> {
        val decryptedFilePath = createCopyOfOriginalFile(originalFilePath)

        return Observable.create { emitter ->

            try {

                val fis = FileInputStream(originalFilePath)

                try {
                    val aes = Cipher.getInstance(ALGORITHM)
                    aes.init(Cipher.DECRYPT_MODE, key)
                    val out = CipherInputStream(fis, aes)

                    File(decryptedFilePath).outputStream().use {
                        out.copyTo(it)
                    }

                } catch (ex: NoSuchAlgorithmException) {
                    ex.printStackTrace()

                    if (!emitter.isDisposed) {
                        emitter.onError(ex)
                        emitter.onComplete()
                    }
                } catch (ex: IOException) {
                    ex.printStackTrace()

                    if (!emitter.isDisposed) {
                        emitter.onError(ex)
                        emitter.onComplete()
                    }
                }

            } catch (e: Exception) {
                e.printStackTrace()

                if (!emitter.isDisposed) {
                    emitter.onError(e)
                    emitter.onComplete()
                }
            }

            if (!emitter.isDisposed) {
                emitter.onNext(File(decryptedFilePath))
                emitter.onComplete()
            }
        }
    }

    private fun createCopyOfOriginalFile(originalFilePath: String): String {

        val filePath = getImageParentPath(originalFilePath)
        val imageName = getImageNameFromPath(originalFilePath)

        val originalFile = File(originalFilePath)
        val copyFile = File(filePath, "$TEMP_IMAGE_TAG$imageName")
        copy(originalFile, copyFile)

        return copyFile.path
    }

    @Throws(IOException::class)
    fun copy(sourceLocation: File, targetLocation: File) {
        if (sourceLocation.isDirectory) {
            copyDirectory(sourceLocation, targetLocation)
        } else {
            copyFile(sourceLocation, targetLocation)
        }
    }

    @Throws(IOException::class)
    private fun copyDirectory(source: File, target: File) {
        if (!target.exists()) {
            target.mkdir()
        }

        for (f in source.list()) {
            copy(File(source, f), File(target, f))
        }
    }

    @Throws(IOException::class, FileNotFoundException::class)
    private fun copyFile(source: File, target: File) {
        try {
            val inputStream = FileInputStream(source)
            val out = FileOutputStream(target)
            out.write(inputStream.readBytes())
            out.flush()
            out.close()
        } catch (e: IOException) {
            throw IOException()
        }
    }

    private fun getImageParentPath(path: String?): String {
        var newPath = ""
        path?.let {
            newPath = it.substring(0, it.lastIndexOf("/") + 1)
        }
        return newPath
    }

    private fun getImageNameFromPath(path: String?): String {
        var newPath = ""
        path?.let {
            newPath = it.substring(it.lastIndexOf("/") + 1)
        }
        return newPath
    }

    private fun deleteFile(path: String): Boolean {
        val f = File(path)

        return if (f.exists()) {
            f.delete()
        } else false
    }

    private fun renameImageToOriginalFileName(path: String): String {
        val filePath = getImageParentPath(path)
        val imageName = getImageNameFromPath(path)

        val from = File(filePath, imageName)

        val renameTo = imageName.replace(TEMP_IMAGE_TAG, "")

        val to = File(filePath, renameTo)
        if (from.exists()) {
            from.renameTo(to)
        }

        return to.path
    }
}