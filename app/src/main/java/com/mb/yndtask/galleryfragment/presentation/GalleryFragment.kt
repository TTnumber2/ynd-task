package com.mb.yndtask.galleryfragment.presentation

import android.Manifest.permission.CAMERA
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.RecyclerView
import com.mb.yndtask.R
import com.mb.yndtask.basedi.ModuleComponentProvider
import com.mb.yndtask.basepresentation.autoRelease
import com.mb.yndtask.databinding.FragmentGalleryBinding
import com.mb.yndtask.db.photo.model.Photo
import com.mb.yndtask.extensions.onUpDownScrollListener
import com.mb.yndtask.galleryfragment.presentation.adapterview.PhotoItem
import com.mb.yndtask.galleryfragment.viewmodel.GalleryFragmentMVVM
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.GenericItem
import com.mikepenz.fastadapter.adapters.ItemAdapter
import java.io.File
import java.util.UUID
import java.util.jar.Manifest
import javax.inject.Inject

class GalleryFragment : Fragment() {

    @Inject
    lateinit var viewModel: GalleryFragmentMVVM.ViewModel

    private var binding: FragmentGalleryBinding by autoRelease()
    private val args: GalleryFragmentArgs by navArgs()
    private val itemAdapter = ItemAdapter<GenericItem>()

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentGalleryBinding.inflate(inflater, container, false).apply {
            adapter = FastAdapter.with(itemAdapter)
            addPhotoClick = View.OnClickListener { askForCameraPermissionIfNeeded() }
            list.onUpDownScrollListener({ addPhoto.shrink() }, { addPhoto.extend() })
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ModuleComponentProvider.provideModuleComponent().plusGalleryFragmentComponentBuilder()
                .bindLoggedUser(args.username)
                .build().inject(this)

        lifecycle.addObserver(viewModel)

        observePhotoListLiveData()
    }

    private fun observePhotoListLiveData() {
        viewModel.photosLiveData.observe(viewLifecycleOwner, Observer {
            itemAdapter.clear()

            it.forEach {
                itemAdapter.add(PhotoItem(it))
            }

            binding.adapter?.notifyAdapterDataSetChanged()
        })
    }

    private fun askForCameraPermissionIfNeeded() {
        requireActivity().let { activity ->
            activity.registerForActivityResult(ActivityResultContracts.RequestPermission()) {
                it?.let {
                    if (it) {
                        takePhoto()
                    }
                }
            }.launch(CAMERA)
        }
    }

    private fun takePhoto() {
        val time = System.currentTimeMillis()
        val photoName = "$time.jpg"
        val photoPath = requireContext().filesDir.toString() + File.separator + photoName
        val uri = FileProvider.getUriForFile(requireContext(), requireContext().applicationContext.packageName + ".fileprovider", File(photoPath))
        val photo = Photo(0L, 0L, photoPath, time, UUID.randomUUID().toString())

        activity?.registerForActivityResult(ActivityResultContracts.TakePicture()) {
            if (it) {
                viewModel.savePhoto(photo)
            }
        }?.launch(uri)
    }
}