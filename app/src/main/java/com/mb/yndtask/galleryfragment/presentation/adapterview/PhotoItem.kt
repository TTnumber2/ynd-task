package com.mb.yndtask.galleryfragment.presentation.adapterview

import android.graphics.drawable.Drawable
import android.net.Uri
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.mb.yndtask.R
import com.mb.yndtask.basedi.ModuleComponentProvider
import com.mb.yndtask.galleryfragment.domain.model.PhotoDomain
import com.mb.yndtask.galleryfragment.domain.photoencrypt.PhotoSecretImpl
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.item_photo.view.*
import org.joda.time.DateTime
import java.io.File

class PhotoItem(private val photo: PhotoDomain) : AbstractItem<PhotoItem.ViewHolder>() {

    override val type: Int = R.id.photo_item_id
    override val layoutRes: Int = R.layout.item_photo
    override fun getViewHolder(v: View): ViewHolder = ViewHolder(v)

    class ViewHolder(itemView: View) : FastAdapter.ViewHolder<PhotoItem>(itemView) {

        private val compositeDisposable = CompositeDisposable()

        override fun unbindView(item: PhotoItem) {
            itemView.photo_creation_date.text = null
            itemView.photo_uuid.text = null
            itemView.photo_path.text = null
            itemView.photo_preview.setImageDrawable(null)
            itemView.progress.visibility = View.VISIBLE

            compositeDisposable.clear()
        }

        override fun bindView(item: PhotoItem, payloads: List<Any>) {
            itemView.photo_creation_date.text = DateTime.now().withMillis(item.photo.photoCreationDate).toString("dd-MM-yyyy HH:mm")
            itemView.photo_uuid.text = item.photo.photoUUID
            itemView.photo_path.text = item.photo.photoPath

            decryptImage(item)
        }

        private fun decryptImage(item: PhotoItem) {
            try {
                PhotoSecretImpl().decryptImage(item.photo.photoPath)
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                                       loadFileIntoGlide(it)
                                   }, {}).apply { compositeDisposable.add(this) }
            } catch (e: Exception) {

            }
        }

        private fun loadFileIntoGlide(file: File) {
            Glide.with(itemView.context)
                    .load(Uri.fromFile(file))
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            itemView.progress.visibility = View.GONE
                            file.delete()
                            return false
                        }
                    })
                    .into(itemView.photo_preview)
        }
    }
}