package com.mb.yndtask.galleryfragment.viewmodel

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import com.mb.yndtask.db.photo.model.Photo
import com.mb.yndtask.galleryfragment.domain.model.PhotoDomain

interface GalleryFragmentMVVM {

    interface ViewModel : LifecycleObserver {
        val photosLiveData: LiveData<List<PhotoDomain>>

        fun savePhoto(photo: Photo)
    }
}