package com.mb.yndtask.galleryfragment.viewmodel

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import com.mb.yndtask.basedi.module.SchedulerModule.SchedulerIO
import com.mb.yndtask.basedi.module.SchedulerModule.SchedulerUI
import com.mb.yndtask.db.photo.model.Photo
import com.mb.yndtask.galleryfragment.domain.GalleryInteractor
import com.mb.yndtask.galleryfragment.domain.model.PhotoDomain
import com.mb.yndtask.galleryfragment.domain.photoencrypt.PhotoSecret
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class GalleryViewModel
@Inject constructor(private val interactor: GalleryInteractor,
                    private val photoSecret: PhotoSecret,
                    @SchedulerIO private val schedulerIO: Scheduler,
                    @SchedulerUI private val schedulerUI: Scheduler) : ViewModel(), GalleryFragmentMVVM.ViewModel {

    private val compositeDisposable = CompositeDisposable()

    private val _photosLiveData: MutableLiveData<List<PhotoDomain>> = MutableLiveData()
    override val photosLiveData: LiveData<List<PhotoDomain>> = _photosLiveData

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume() {
        getUserPhotos()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onPause() {
        compositeDisposable.clear()
    }

    private fun getUserPhotos() {
        interactor.getPhotosForUser()
                .subscribeOn(schedulerIO)
                .observeOn(schedulerUI)
                .subscribe({
                               _photosLiveData.postValue(it)
                           }, {})
                .apply { compositeDisposable.add(this) }
    }

    override fun savePhoto(photo: Photo) {
        interactor.savePhotoForUser(photo)
                .flatMapCompletable { photoSecret.encryptImage(photo.photoPath) }
                .subscribeOn(schedulerIO)
                .observeOn(schedulerUI)
                .subscribe({
                               getUserPhotos()
                           }, {
                    val test = ""
                })
                .apply { compositeDisposable.add(this) }
    }
}