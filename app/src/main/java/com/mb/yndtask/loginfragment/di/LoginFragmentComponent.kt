package com.mb.yndtask.loginfragment.di

import com.mb.yndtask.loginfragment.presentation.LoginFragment
import dagger.Subcomponent

@Subcomponent(modules = [LoginFragmentPresentationModule::class, LoginFragmentDomainModule::class])
interface LoginFragmentComponent {

    fun inject(fragment: LoginFragment)
}