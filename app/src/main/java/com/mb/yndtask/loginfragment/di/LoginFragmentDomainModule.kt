package com.mb.yndtask.loginfragment.di

import com.mb.yndtask.loginfragment.domain.LoginInteractor
import com.mb.yndtask.loginfragment.domain.LoginInteractorImpl
import dagger.Binds
import dagger.Module

@Module
abstract class LoginFragmentDomainModule {

    @Binds
    abstract fun bindsInteractor(interactor: LoginInteractorImpl): LoginInteractor
}