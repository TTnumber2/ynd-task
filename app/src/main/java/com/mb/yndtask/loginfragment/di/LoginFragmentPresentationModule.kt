package com.mb.yndtask.loginfragment.di

import com.mb.yndtask.loginfragment.viewmodel.LoginFragmentMVVM
import com.mb.yndtask.loginfragment.viewmodel.LoginFragmentViewModel
import dagger.Binds
import dagger.Module

@Module
abstract class LoginFragmentPresentationModule {

    @Binds
    abstract fun bindsViewModel(vm: LoginFragmentViewModel): LoginFragmentMVVM.ViewModel
}