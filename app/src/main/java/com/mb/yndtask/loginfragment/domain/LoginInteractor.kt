package com.mb.yndtask.loginfragment.domain

import com.mb.yndtask.db.user.model.User
import io.reactivex.Single

interface LoginInteractor {

    fun tryToSignIn(username: String): Single<User>
}