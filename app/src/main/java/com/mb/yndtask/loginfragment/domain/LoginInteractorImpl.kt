package com.mb.yndtask.loginfragment.domain

import com.mb.yndtask.db.user.dao.UserDAO
import javax.inject.Inject

class LoginInteractorImpl
@Inject constructor(private val userDAO: UserDAO) : LoginInteractor {

    override fun tryToSignIn(username: String) =
            userDAO.getUser(username)

}