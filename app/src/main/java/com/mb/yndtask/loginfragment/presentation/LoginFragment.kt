package com.mb.yndtask.loginfragment.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.mb.yndtask.R
import com.mb.yndtask.basedi.ModuleComponentProvider
import com.mb.yndtask.basepresentation.autoRelease
import com.mb.yndtask.basepresentation.dialog.ErrorDialog
import com.mb.yndtask.databinding.FragmentLoginBinding
import com.mb.yndtask.loginfragment.viewmodel.LoginFragmentMVVM
import com.mb.yndtask.loginfragment.viewmodel.model.SignInAction
import javax.inject.Inject

class LoginFragment : Fragment() {

    @Inject
    lateinit var viewModel: LoginFragmentMVVM.ViewModel

    private var binding: FragmentLoginBinding by autoRelease()

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentLoginBinding.inflate(inflater, container, false).apply {
            signUpClick = View.OnClickListener { findNavController().navigate(LoginFragmentDirections.actionFragmentLoginToFragmentRegister()) }
            signInClick = View.OnClickListener {
                viewModel.tryToSignIn(binding.userNameEditText.getText(), binding.passwordEditText.getText())
            }
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ModuleComponentProvider.provideModuleComponent().plusLoginFragmentComponent().inject(this)

        lifecycle.addObserver(viewModel)
        observeSignInActionLiveData()
        observeSignInSuccessLiveData()
    }

    private fun observeSignInSuccessLiveData() {
        viewModel.signInSuccessLiveData.observe(viewLifecycleOwner, Observer {
            binding.userNameEditText.setEmptyText()
            binding.passwordEditText.setEmptyText()
            findNavController().navigate(LoginFragmentDirections.actionFragmentLoginToFragmentGallery(it.login))
        })
    }

    private fun observeSignInActionLiveData() {
        viewModel.actionLiveData.observe(viewLifecycleOwner, Observer {
            when (it) {
                SignInAction.USER_EXISTS_ERROR -> showErrorDialog(getString(R.string.user_not_exists_title), getString(R.string.user_not_exists_message))
                SignInAction.WRONG_PASSWORD_ERROR -> showErrorDialog(getString(R.string.password_wrong_title), getString(R.string.password_wrong_message))
            }
        })
    }

    private fun showErrorDialog(title: String, message: String) {
        ErrorDialog(requireContext())
                .showWithInstance()
                .setTitleText(title)
                .setMessageText(message)
    }
}