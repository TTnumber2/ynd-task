package com.mb.yndtask.loginfragment.viewmodel

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import com.mb.yndtask.db.user.model.User
import com.mb.yndtask.loginfragment.viewmodel.model.SignInAction

interface LoginFragmentMVVM {

    interface ViewModel: LifecycleObserver {
        val actionLiveData: LiveData<SignInAction>
        val signInSuccessLiveData: LiveData<User>

        fun tryToSignIn(username: String, password: String)
    }
}