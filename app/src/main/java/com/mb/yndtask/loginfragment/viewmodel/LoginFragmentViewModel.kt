package com.mb.yndtask.loginfragment.viewmodel

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import com.mb.yndtask.basedi.module.SchedulerModule.SchedulerIO
import com.mb.yndtask.basedi.module.SchedulerModule.SchedulerUI
import com.mb.yndtask.db.user.model.User
import com.mb.yndtask.loginfragment.domain.LoginInteractor
import com.mb.yndtask.loginfragment.viewmodel.model.SignInAction
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class LoginFragmentViewModel
@Inject constructor(private val interactor: LoginInteractor,
                    @SchedulerIO private val schedulerIO: Scheduler,
                    @SchedulerUI private val schedulerUI: Scheduler) : ViewModel(), LoginFragmentMVVM.ViewModel {

    private val compositeDisposable = CompositeDisposable()

    private val _actionLiveData: MutableLiveData<SignInAction> = MutableLiveData()
    override val actionLiveData: LiveData<SignInAction> = _actionLiveData

    private val _signInSuccessLiveData: MutableLiveData<User> = MutableLiveData()
    override val signInSuccessLiveData: LiveData<User> = _signInSuccessLiveData

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onPause() {
        compositeDisposable.clear()
    }

    override fun tryToSignIn(username: String, password: String) {
        interactor.tryToSignIn(username)
                .subscribeOn(schedulerIO)
                .observeOn(schedulerUI)
                .subscribe({
                               if (it.password != password) {
                                   _actionLiveData.postValue(SignInAction.WRONG_PASSWORD_ERROR)
                                   return@subscribe
                               }
                               _signInSuccessLiveData.postValue(it)
                           }, {
                               _actionLiveData.postValue(SignInAction.USER_EXISTS_ERROR)
                           })
                .apply { compositeDisposable.add(this) }
    }
}