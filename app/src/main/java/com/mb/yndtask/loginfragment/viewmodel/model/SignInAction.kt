package com.mb.yndtask.loginfragment.viewmodel.model

enum class SignInAction {

    USER_EXISTS_ERROR,
    WRONG_PASSWORD_ERROR
}