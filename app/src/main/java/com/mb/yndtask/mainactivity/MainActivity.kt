package com.mb.yndtask.mainactivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.mb.yndtask.R
import com.mb.yndtask.basedi.ModuleComponentProvider

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ModuleComponentProvider.buildModuleComponent(this)
    }
}