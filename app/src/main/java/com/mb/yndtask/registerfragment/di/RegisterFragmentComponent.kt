package com.mb.yndtask.registerfragment.di

import com.mb.yndtask.registerfragment.presentation.RegisterFragment
import dagger.Subcomponent

@Subcomponent(modules = [RegisterFragmentViewModelModule::class, RegisterFragmentDomainModule::class])
interface RegisterFragmentComponent {

    fun inject(fragment: RegisterFragment)
}