package com.mb.yndtask.registerfragment.di

import com.mb.yndtask.registerfragment.domain.RegisterInteractor
import com.mb.yndtask.registerfragment.domain.RegisterInteractorImpl
import dagger.Binds
import dagger.Module

@Module
abstract class RegisterFragmentDomainModule {

    @Binds
    abstract fun bindInteractor(interactor: RegisterInteractorImpl): RegisterInteractor
}