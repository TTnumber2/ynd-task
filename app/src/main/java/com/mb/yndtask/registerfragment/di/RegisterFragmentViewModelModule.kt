package com.mb.yndtask.registerfragment.di

import com.mb.yndtask.registerfragment.viewmodel.RegisterFragmentMVVM
import com.mb.yndtask.registerfragment.viewmodel.RegisterFragmentViewModel
import dagger.Binds
import dagger.Module

@Module
abstract class RegisterFragmentViewModelModule {

    @Binds
    abstract fun bindsViewModel(vm: RegisterFragmentViewModel): RegisterFragmentMVVM.ViewModel
}