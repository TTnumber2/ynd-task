package com.mb.yndtask.registerfragment.domain

import com.mb.yndtask.db.user.model.User
import io.reactivex.Single

interface RegisterInteractor {

    fun checkForUser(username: String): Single<User>
    fun signUp(username: String, password: String): Single<User>
}