package com.mb.yndtask.registerfragment.domain

import com.mb.yndtask.db.user.dao.UserDAO
import com.mb.yndtask.db.user.model.User
import io.reactivex.Single
import javax.inject.Inject

class RegisterInteractorImpl
@Inject constructor(private val userDAO: UserDAO) : RegisterInteractor {

    override fun checkForUser(username: String) =
            userDAO.getUser(username)

    override fun signUp(username: String, password: String) =
            Single.just(User(0L, username, password))
                    .map {
                        userDAO.insert(it)
                        it
                    }
}