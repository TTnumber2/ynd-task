package com.mb.yndtask.registerfragment.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.mb.yndtask.R
import com.mb.yndtask.basedi.ModuleComponentProvider
import com.mb.yndtask.basepresentation.autoRelease
import com.mb.yndtask.basepresentation.dialog.ErrorDialog
import com.mb.yndtask.databinding.FragmentRegisterBinding
import com.mb.yndtask.registerfragment.viewmodel.RegisterFragmentMVVM
import com.mb.yndtask.registerfragment.viewmodel.model.SignUpAction
import javax.inject.Inject

class RegisterFragment : Fragment() {

    @Inject
    lateinit var viewModel: RegisterFragmentMVVM.ViewModel

    private var binding: FragmentRegisterBinding by autoRelease()

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentRegisterBinding.inflate(inflater, container, false).apply {
            signUpClick = View.OnClickListener {
                viewModel.tryToSignUp(binding.userNameEditText.getText(),
                                      binding.passwordEditText.getText(),
                                      binding.passwordConfirmEditText.getText())
            }
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ModuleComponentProvider.provideModuleComponent().plusRegisterFragmentComponent().inject(this)

        lifecycle.addObserver(viewModel)
        observeUsernameValidateLiveData()
        observePasswordValidateLiveData()
        observeSignUpActionLiveData()
        viewModel.setUsernameTextWatcherObservable(binding.userNameEditText.getTextWatcherObservable())
        viewModel.setPasswordTextWatcherObservable(binding.passwordEditText.getTextWatcherObservable())
    }

    private fun observeUsernameValidateLiveData() {
        viewModel.usernameValidateLiveData.observe(viewLifecycleOwner, Observer {
            binding.usernameLengthCheckbox.setChecked(it.isProperLength)
            binding.usernameSpecialCharsCheckbox.setChecked(it.hasSpecialCharacters.not())
        })
    }

    private fun observePasswordValidateLiveData() {
        viewModel.passwordValidateLiveData.observe(viewLifecycleOwner, Observer {
            binding.passwordLengthCheckbox.setChecked(it.isProperLength)
            binding.passwordSpecialCharsCheckbox.setChecked(it.hasSpecialCharacters)
            binding.passwordBigCharCheckbox.setChecked(it.hasAtLeastOneBigChar)
        })
    }

    private fun observeSignUpActionLiveData() {
        viewModel.actionLiveData.observe(viewLifecycleOwner, Observer {
            when (it) {
                SignUpAction.USERNAME_ERROR -> showErrorDialog(getString(R.string.username_is_not_valid_title), getString(R.string.username_is_not_valid_message))
                SignUpAction.PASSWORD_ERROR -> showErrorDialog(getString(R.string.password_is_not_valid_title), getString(R.string.password_is_not_valid_message))
                SignUpAction.PASSWORD_NOT_EQUAL_ERROR -> showErrorDialog(getString(R.string.password_not_equal_title), getString(R.string.password_not_equal_message))
                SignUpAction.USER_EXISTS_ERROR -> showErrorDialog(getString(R.string.user_exists_title), getString(R.string.user_exists_message))
                SignUpAction.SIGN_UP_SUCCESS -> findNavController().navigateUp()
            }
        })
    }

    private fun showErrorDialog(title: String, message: String) {
        ErrorDialog(requireContext())
                .showWithInstance()
                .setTitleText(title)
                .setMessageText(message)
    }
}