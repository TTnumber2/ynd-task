package com.mb.yndtask.registerfragment.viewmodel

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import com.jakewharton.rxbinding2.InitialValueObservable
import com.jakewharton.rxbinding2.widget.TextViewTextChangeEvent
import com.mb.yndtask.registerfragment.viewmodel.model.PasswordValidate
import com.mb.yndtask.registerfragment.viewmodel.model.SignUpAction
import com.mb.yndtask.registerfragment.viewmodel.model.UsernameValidate
import io.reactivex.Observable

interface RegisterFragmentMVVM {

    interface ViewModel: LifecycleObserver {
        val usernameValidateLiveData: LiveData<UsernameValidate>
        val passwordValidateLiveData: LiveData<PasswordValidate>
        val actionLiveData: LiveData<SignUpAction>

        fun setUsernameTextWatcherObservable(observable: Observable<String>)
        fun setPasswordTextWatcherObservable(observable: Observable<String>)
        fun tryToSignUp(username: String, password: String, passwordConfirm: String)
    }
}