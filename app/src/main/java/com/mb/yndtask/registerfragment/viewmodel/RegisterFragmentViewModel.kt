package com.mb.yndtask.registerfragment.viewmodel

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import com.jakewharton.rxbinding2.widget.TextViewTextChangeEvent
import com.mb.yndtask.basedi.module.SchedulerModule.SchedulerIO
import com.mb.yndtask.basedi.module.SchedulerModule.SchedulerUI
import com.mb.yndtask.registerfragment.domain.RegisterInteractor
import com.mb.yndtask.registerfragment.viewmodel.model.PasswordValidate
import com.mb.yndtask.registerfragment.viewmodel.model.SignUpAction
import com.mb.yndtask.registerfragment.viewmodel.model.UsernameValidate
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class RegisterFragmentViewModel
@Inject constructor(private val interactor: RegisterInteractor,
                    @SchedulerIO private val schedulerIO: Scheduler,
                    @SchedulerUI private val schedulerUI: Scheduler) : ViewModel(), RegisterFragmentMVVM.ViewModel {

    private val compositeDisposable = CompositeDisposable()

    private val _usernameValidateLiveData: MutableLiveData<UsernameValidate> = MutableLiveData()
    override val usernameValidateLiveData: LiveData<UsernameValidate> = _usernameValidateLiveData

    private val _passwordValidateLiveData: MutableLiveData<PasswordValidate> = MutableLiveData()
    override val passwordValidateLiveData: LiveData<PasswordValidate> = _passwordValidateLiveData

    private val _actionLiveData: MutableLiveData<SignUpAction> = MutableLiveData()
    override val actionLiveData: LiveData<SignUpAction> = _actionLiveData

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onPause() {
        compositeDisposable.clear()
    }

    override fun setUsernameTextWatcherObservable(observable: Observable<String>) {
        observable.observeOn(schedulerIO)
                .map { text ->
                    UsernameValidate(text.matches("^.{8,30}$".toRegex()),
                                     text.matches("^(.*[!@#\$%^&+=].*)$".toRegex()))
                }
                .observeOn(schedulerUI)
                .subscribe {
                    _usernameValidateLiveData.postValue(it)
                }
                .apply { compositeDisposable.add(this) }
    }

    override fun setPasswordTextWatcherObservable(observable: Observable<String>) {
        observable.observeOn(schedulerIO)
                .map { text ->
                    PasswordValidate(text.matches("^.{4,30}$".toRegex()),
                                     text.matches("^(.*[A-Z].*)$".toRegex()),
                                     text.matches("^(.*[!@#\$%^&+=].*)$".toRegex()))
                }
                .observeOn(schedulerUI)
                .subscribe {
                    _passwordValidateLiveData.postValue(it)
                }
                .apply { compositeDisposable.add(this) }
    }

    override fun tryToSignUp(username: String, password: String, passwordConfirm: String) {
        if (_usernameValidateLiveData.value?.isValid()?.not() == true) {
            _actionLiveData.postValue(SignUpAction.USERNAME_ERROR)
            return
        }

        if (_passwordValidateLiveData.value?.isValid()?.not() == true) {
            _actionLiveData.postValue(SignUpAction.PASSWORD_ERROR)
            return
        }

        if (password != passwordConfirm) {
            _actionLiveData.postValue(SignUpAction.PASSWORD_NOT_EQUAL_ERROR)
            return
        }

        interactor.checkForUser(username)
                .subscribeOn(schedulerIO)
                .observeOn(schedulerUI)
                .subscribe({
                               _actionLiveData.postValue(SignUpAction.USER_EXISTS_ERROR)
                           }, {
                               signUpUser(username, password)
                           })
                .apply { compositeDisposable.add(this) }
    }

    private fun signUpUser(username: String, password: String) {
        interactor.signUp(username, password)
                .subscribeOn(schedulerIO)
                .observeOn(schedulerUI)
                .subscribe({
                               _actionLiveData.postValue(SignUpAction.SIGN_UP_SUCCESS)
                           }, { })
                .apply { compositeDisposable.add(this) }
    }
}