package com.mb.yndtask.registerfragment.viewmodel.model

data class PasswordValidate(val isProperLength: Boolean,
                            val hasAtLeastOneBigChar: Boolean,
                            val hasSpecialCharacters: Boolean) {

    fun isValid(): Boolean = isProperLength && hasAtLeastOneBigChar && hasSpecialCharacters
}