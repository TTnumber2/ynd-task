package com.mb.yndtask.registerfragment.viewmodel.model

enum class SignUpAction {

    USERNAME_ERROR,
    PASSWORD_ERROR,
    PASSWORD_NOT_EQUAL_ERROR,
    USER_EXISTS_ERROR,
    SIGN_UP_SUCCESS
}