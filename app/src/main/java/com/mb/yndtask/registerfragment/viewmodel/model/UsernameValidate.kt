package com.mb.yndtask.registerfragment.viewmodel.model

data class UsernameValidate(val isProperLength: Boolean,
                            val hasSpecialCharacters: Boolean) {

    fun isValid() = isProperLength && hasSpecialCharacters.not()
}