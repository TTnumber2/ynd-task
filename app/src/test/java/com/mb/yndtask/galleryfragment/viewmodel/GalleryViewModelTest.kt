package com.mb.yndtask.galleryfragment.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.mb.yndtask.db.photo.model.Photo
import com.mb.yndtask.galleryfragment.domain.GalleryInteractor
import com.mb.yndtask.galleryfragment.domain.model.PhotoDomain
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.assertj.core.api.Assertions.assertThat
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class GalleryViewModelTest {

    @JvmField
    @Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val interactorMock: GalleryInteractor = mock()
    private val tested = GalleryViewModel(interactorMock, Schedulers.trampoline(), Schedulers.trampoline())

    @Test
    fun shouldShowPhotosForUser() {
        val expected = listOf(PhotoDomain("uuid", "path", 100))
        whenever(interactorMock.getPhotosForUser()).thenReturn(Single.just(expected))

        tested.onResume()

        assertThat(tested.photosLiveData.value).isNotNull
        assertThat(tested.photosLiveData.value).isEqualTo(expected)
    }

    @Test
    fun savedPhotoShouldTriggerUpdatePhotos() {
        val expected = listOf(PhotoDomain("uuid", "path", 100))
        val photo = Photo(0L, 0L, "path", 100, "uuid")
        whenever(interactorMock.getPhotosForUser()).thenReturn(Single.just(expected))
        whenever(interactorMock.savePhotoForUser(photo)).thenReturn(Single.just(1L))

        tested.savePhoto(photo)

        verify(interactorMock).getPhotosForUser()
    }
}