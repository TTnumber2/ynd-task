package com.mb.yndtask.loginfragment.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.mb.yndtask.db.user.model.User
import com.mb.yndtask.loginfragment.domain.LoginInteractor
import com.mb.yndtask.loginfragment.viewmodel.model.SignInAction
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import java.lang.IllegalStateException

class LoginFragmentViewModelTest {

    @JvmField
    @Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val interactorMock: LoginInteractor = mock()
    private val tested = LoginFragmentViewModel(interactorMock, Schedulers.trampoline(), Schedulers.trampoline())

    @Test
    fun shouldShowUserExistsErrorUponSignIn() {
        whenever(interactorMock.tryToSignIn("")).thenReturn(Single.error(IllegalStateException()))

        tested.tryToSignIn("", "")

        assertThat(tested.actionLiveData.value).isNotNull
        assertThat(tested.actionLiveData.value).isEqualTo(SignInAction.USER_EXISTS_ERROR)
    }

    @Test
    fun shouldShowWrongPasswordError() {
        whenever(interactorMock.tryToSignIn("12345678")).thenReturn(Single.just(User(1L, "12345678", "12345678!L")))

        tested.tryToSignIn("12345678", "Test123!")

        assertThat(tested.actionLiveData.value).isNotNull
        assertThat(tested.actionLiveData.value).isEqualTo(SignInAction.WRONG_PASSWORD_ERROR)
    }

    @Test
    fun shouldSignIn() {
        val expected = User(1L, "12345678", "Test123!")
        whenever(interactorMock.tryToSignIn("12345678")).thenReturn(Single.just(expected))

        tested.tryToSignIn("12345678", "Test123!")

        assertThat(tested.actionLiveData.value).isNull()
        assertThat(tested.signInSuccessLiveData.value).isNotNull
        assertThat(tested.signInSuccessLiveData.value).isEqualTo(expected)
    }

}