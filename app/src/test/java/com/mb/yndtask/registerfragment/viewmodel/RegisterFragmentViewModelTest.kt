package com.mb.yndtask.registerfragment.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.mb.yndtask.db.user.model.User
import com.mb.yndtask.registerfragment.domain.RegisterInteractor
import com.mb.yndtask.registerfragment.viewmodel.model.SignUpAction
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule


class RegisterFragmentViewModelTest {

    @JvmField
    @Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val interactorMock: RegisterInteractor = mock()
    private val tested = RegisterFragmentViewModel(interactorMock, Schedulers.trampoline(), Schedulers.trampoline())

    @Test
    fun shouldInitUsernameObservable() {
        val publishSubject = PublishSubject.create<String>()
        val observable: Observable<String> = publishSubject.hide()

        tested.setUsernameTextWatcherObservable(observable)

        publishSubject.onNext("")

        assertThat(tested.usernameValidateLiveData.value).isNotNull
        assertThat(tested.usernameValidateLiveData.value!!.hasSpecialCharacters).isFalse()
        assertThat(tested.usernameValidateLiveData.value!!.isProperLength).isFalse()
    }

    @Test
    fun shouldValidateUsernameByLength() {
        val publishSubject = PublishSubject.create<String>()
        val observable: Observable<String> = publishSubject.hide()

        tested.setUsernameTextWatcherObservable(observable)

        publishSubject.onNext("12345678")

        assertThat(tested.usernameValidateLiveData.value).isNotNull
        assertThat(tested.usernameValidateLiveData.value!!.hasSpecialCharacters).isFalse()
        assertThat(tested.usernameValidateLiveData.value!!.isProperLength).isTrue()
    }

    @Test
    fun shouldValidateUsernameBySpecialCharacters() {
        val publishSubject = PublishSubject.create<String>()
        val observable: Observable<String> = publishSubject.hide()

        tested.setUsernameTextWatcherObservable(observable)

        publishSubject.onNext("!")

        assertThat(tested.usernameValidateLiveData.value).isNotNull
        assertThat(tested.usernameValidateLiveData.value!!.hasSpecialCharacters).isTrue()
        assertThat(tested.usernameValidateLiveData.value!!.isProperLength).isFalse()
    }

    @Test
    fun shouldValidateUsernameByLengthAndSpecialCharacters() {
        val publishSubject = PublishSubject.create<String>()
        val observable: Observable<String> = publishSubject.hide()

        tested.setUsernameTextWatcherObservable(observable)

        publishSubject.onNext("12345678!")

        assertThat(tested.usernameValidateLiveData.value).isNotNull
        assertThat(tested.usernameValidateLiveData.value!!.hasSpecialCharacters).isTrue()
        assertThat(tested.usernameValidateLiveData.value!!.isProperLength).isTrue()
    }

    @Test
    fun shouldInitPasswordObservable() {
        val publishSubject = PublishSubject.create<String>()
        val observable: Observable<String> = publishSubject.hide()

        tested.setPasswordTextWatcherObservable(observable)

        publishSubject.onNext("")

        assertThat(tested.passwordValidateLiveData.value).isNotNull
        assertThat(tested.passwordValidateLiveData.value!!.hasSpecialCharacters).isFalse()
        assertThat(tested.passwordValidateLiveData.value!!.isProperLength).isFalse()
        assertThat(tested.passwordValidateLiveData.value!!.hasAtLeastOneBigChar).isFalse()
    }

    @Test
    fun shouldValidatePasswordByLength() {
        val publishSubject = PublishSubject.create<String>()
        val observable: Observable<String> = publishSubject.hide()

        tested.setPasswordTextWatcherObservable(observable)

        publishSubject.onNext("1234")

        assertThat(tested.passwordValidateLiveData.value).isNotNull
        assertThat(tested.passwordValidateLiveData.value!!.hasSpecialCharacters).isFalse()
        assertThat(tested.passwordValidateLiveData.value!!.isProperLength).isTrue()
        assertThat(tested.passwordValidateLiveData.value!!.hasAtLeastOneBigChar).isFalse()
    }

    @Test
    fun shouldValidatePasswordBySpecialCharacters() {
        val publishSubject = PublishSubject.create<String>()
        val observable: Observable<String> = publishSubject.hide()

        tested.setPasswordTextWatcherObservable(observable)

        publishSubject.onNext("!")

        assertThat(tested.passwordValidateLiveData.value).isNotNull
        assertThat(tested.passwordValidateLiveData.value!!.hasSpecialCharacters).isTrue()
        assertThat(tested.passwordValidateLiveData.value!!.isProperLength).isFalse()
        assertThat(tested.passwordValidateLiveData.value!!.hasAtLeastOneBigChar).isFalse()
    }

    @Test
    fun shouldValidatePasswordByAtLeastOneBigCharacter() {
        val publishSubject = PublishSubject.create<String>()
        val observable: Observable<String> = publishSubject.hide()

        tested.setPasswordTextWatcherObservable(observable)

        publishSubject.onNext("A")

        assertThat(tested.passwordValidateLiveData.value).isNotNull
        assertThat(tested.passwordValidateLiveData.value!!.hasSpecialCharacters).isFalse()
        assertThat(tested.passwordValidateLiveData.value!!.isProperLength).isFalse()
        assertThat(tested.passwordValidateLiveData.value!!.hasAtLeastOneBigChar).isTrue()
    }

    @Test
    fun shouldValidatePassword() {
        val publishSubject = PublishSubject.create<String>()
        val observable: Observable<String> = publishSubject.hide()

        tested.setPasswordTextWatcherObservable(observable)

        publishSubject.onNext("A123456!")

        assertThat(tested.passwordValidateLiveData.value).isNotNull
        assertThat(tested.passwordValidateLiveData.value!!.hasSpecialCharacters).isTrue()
        assertThat(tested.passwordValidateLiveData.value!!.isProperLength).isTrue()
        assertThat(tested.passwordValidateLiveData.value!!.hasAtLeastOneBigChar).isTrue()
    }

    @Test
    fun shouldShowUsernameValidationErrorOnTryToSignUp() {
        postUsernameText("123!")

        tested.tryToSignUp("username", "password", "password")

        assertThat(tested.actionLiveData.value).isNotNull
        assertThat(tested.actionLiveData.value).isEqualTo(SignUpAction.USERNAME_ERROR)
    }

    @Test
    fun shouldShowPasswordValidationErrorOnTryToSignUp() {
        postUsernameText("12345678")
        postPasswordText("123!")

        tested.tryToSignUp("12345678", "password", "password")

        assertThat(tested.actionLiveData.value).isNotNull
        assertThat(tested.actionLiveData.value).isEqualTo(SignUpAction.PASSWORD_ERROR)
    }

    @Test
    fun shouldShowPasswordNotEqualErrorOnTryToSignUp() {
        postUsernameText("12345678")
        postPasswordText("12345678A!")

        tested.tryToSignUp("12345678", "12345678A!", "12345678A")

        assertThat(tested.actionLiveData.value).isNotNull
        assertThat(tested.actionLiveData.value).isEqualTo(SignUpAction.PASSWORD_NOT_EQUAL_ERROR)
    }

    @Test
    fun shouldShowUserExistsError() {
        whenever(interactorMock.checkForUser(any())).thenReturn(Single.just(User(0L, "", "")))
        postUsernameText("12345678")
        postPasswordText("12345678A!")

        tested.tryToSignUp("12345678", "12345678A!", "12345678A!")

        assertThat(tested.actionLiveData.value).isNotNull
        assertThat(tested.actionLiveData.value).isEqualTo(SignUpAction.USER_EXISTS_ERROR)
    }

    @Test
    fun shouldSignUpSuccessfully() {
        whenever(interactorMock.checkForUser(any())).thenReturn(Single.error(IllegalStateException()))
        whenever(interactorMock.signUp(any(), any())).thenReturn(Single.just(User(1L, "", "")))
        postUsernameText("12345678")
        postPasswordText("12345678A!")

        tested.tryToSignUp("12345678", "12345678A!", "12345678A!")

        assertThat(tested.actionLiveData.value).isNotNull
        assertThat(tested.actionLiveData.value).isEqualTo(SignUpAction.SIGN_UP_SUCCESS)
    }

    private fun postUsernameText(text: String) {
        val publishSubject = PublishSubject.create<String>()
        val observable: Observable<String> = publishSubject.hide()

        tested.setUsernameTextWatcherObservable(observable)

        publishSubject.onNext(text)
    }

    private fun postPasswordText(text: String) {
        val publishSubject = PublishSubject.create<String>()
        val observable: Observable<String> = publishSubject.hide()

        tested.setPasswordTextWatcherObservable(observable)

        publishSubject.onNext(text)
    }
}